## 1.4.2 (2023-05-11)

### bug (1 change)

- [Make entrypoint handling consistent](cassini/gitlab-templates@1447b0878aab894c198e408998f22bf812df8f8a)

## 1.4.1 (2023-05-02)

### bug (1 change)

- [[lava, child] Use CI_REGISTRY_IMAGE in image paths](cassini/gitlab-templates@fc3dac89af4c0e21a4ee2314ba1494537ca67226)

## 1.4.0 (2023-04-25)

### feature (1 change)

- [static-analysis: Add in the use of the cspell checker](cassini/gitlab-templates@070f2988265fb1b7bc948f794f574c08dfb424c1)

## 1.3.0 (2023-04-19)

### other (1 change)

- [ci: Enable the python checkers](cassini/gitlab-templates@62d5891024cdc9ea34eccedee4ed478cd9208539)

### feature (1 change)

- [static-analysis: Add in the use of the clang-format checker](cassini/gitlab-templates@da2476504e908802c6a2cb1b017468fc1ad5732d)

## 1.2.0 (2023-04-12)

### feature (4 changes)

- [common: Retry by default](cassini/gitlab-templates@e30df0f887070f1ade9fd0264d3d9aa6b51911a7)
- [static-analysis: Add in the use of the cmakelint checker](cassini/gitlab-templates@1980397d91ee914cd8b3078589246fa44ec6293b)
- [static-analysis: Add in the use of the oelint-adv checker](cassini/gitlab-templates@e551c28c87776f8b35012dc762128d65c80ccce7)
- [static-analysis: Enable secret detection](cassini/gitlab-templates@9e27f272e2512e45c9dd9025ea360c349a6422d1)

### other (1 change)

- [Switch to expecting format-1 copyright notices](cassini/gitlab-templates@9e33772cd4013b18f6d99ea299af6ec3e792b0f4)

### bug (1 change)

- [common: Correct the tag used on the default job definition](cassini/gitlab-templates@30928929e7088e8ca216c4d206b1501cc569eade)

## 1.1.2 (2023-03-09)

### other (1 change)

- [common: Support to run from a multi-project pipeline](cassini/gitlab-templates@690b6b869271ae9f67fe8d5e37d39092a4934122)

## 1.1.1 (2023-01-26)

### bug (1 change)

- [docker: Need to push the image after building](cassini/gitlab-templates@e9c0f57fb0678ab5cd7d7f0023805914212a0913)

## 1.1.0 (2023-01-25)

### feature (1 change)

- [static-analysis: Add in the use of the inclusivity checker](cassini/gitlab-templates@7dd03452ad8508490c7f29e0f4a0c237c49788e2)

## 1.0.0 (2023-01-24)

### feature (2 changes)

- [docker: Switch to using buildah for docker builds](cassini/gitlab-templates@9b547bdeaf11f021317e9271644c0ff7acef3714)
- [common: New job template for fetch files](cassini/gitlab-templates@3c8582216d8403207d3bd9a612c30db7f9ff0dbd) ([merge request](cassini/gitlab-templates!17))

### other (1 change)

- [lava: Error when job submission fails](cassini/gitlab-templates@e3f44489e0683ef14d0e15a6c4104ef92b06d3af) ([merge request](cassini/gitlab-templates!17))

## 0.0.13 (2022-12-12)

### bug (1 change)

- [static-analysis: Pull latest tag of plugins](cassini/gitlab-templates@46656fdb55fa0bbf4339245588e2af7f6823481a) ([merge request](cassini/gitlab-templates!15))

## 0.0.12 (2022-11-11)

### feature (2 changes)

- [Child pipeline helpers](engineering/embedded-a/templates@2872d98eccbaccf288737d45edb65f3f8c2a9bb0)
- [Add support for fetching version number from json files](engineering/embedded-a/templates@584466404b484d3e31d174680b47832ddf66069e)

### other (1 change)

- [Add changelog generation and releasing to ci](engineering/embedded-a/templates@a98e830e5f95150131beb4151843702731252d50)

## 0.0.11 (2022-11-11)

### other (1 change)

- [Add changelog generation and releasing to ci](engineering/embedded-a/templates@a98e830e5f95150131beb4151843702731252d50)

### feature (2 changes)

- [Add support for fetching version number from json files](engineering/embedded-a/templates@584466404b484d3e31d174680b47832ddf66069e)
- [Add yocto and kas build templates](engineering/embedded-a/templates@f69651997ec05c143c9be9d2eaa6eefede082083) ([merge request](engineering/embedded-a/templates!12))
