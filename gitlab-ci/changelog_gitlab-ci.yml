# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
---
# This job generates a CHANGELOG.md file when an MR that modifies the version
# file is created. This will cause a new commit to the MR and cancel/re-trigger
# the pipeline
#
# This job required the following variables be set
#   CI_BOT_API_TOKEN  User token that will commit the updated changelog file
#   RELEASE_BRANCH    Name of the branch releases should be made from
#                     Defaults to main
#   VERSION_FILE      File containing the packages version information
#
# You also need to provide an image containing any required parsing tools and
# the curl binary
#
# This job supports various version file types by default and new one can be
# added by overriding the before_script
# Ruby:
# extra variable required
#   GEMSPEC_FILE       Projects top level gemspec file
# image required
#   A ruby image such as ruby:2.7
#
# Rust:
#   PACKAGE_NAME       Name of the package in the manifest to get the version
#                      info for
# image required
#   An image containing both cargo and python3 such as the rust/java image
#
# C/C++:
#   DEFINE_NAME:       Name of the define that contains the version number
#
# image required
#   An image containing gcc
#
# YAML:
#   VERSION_VARIABLE:  Name of the variable that contains the version number
#
# image required
#   An image containing yq
#
# JSON:
#   VERSION_VARIABLE:  Name of the variable that contains the version number
#
# image required
#   An image containing jq or an image using apt as the package manager with
#   a jq package available

variables:
  RELEASE_BRANCH:
    value: $CI_DEFAULT_BRANCH
    description: The branch currently being used to create releases

changelog:
  extends: .resource-request
  stage: .pre
  rules:
    - if: '$CI_BOT_API_TOKEN == null'
      when: never
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $RELEASE_BRANCH'
      changes:
        - $VERSION_FILE
  before_script:
    - >
      sed_script="/^#define[ \t]+${DEFINE_NAME}/!d;
      s/#define[ \t]+[^ \t]+[ \t\"]+([^ \t\"]*).*/\1/"
    - |
      echo "Ensuring curl is available"
      if ! [ -x "$(command -v curl)" ]; then
        echo "curl binary not found attempting to install it"
        if [ -x "$(command -v apt)" ]; then
          apt update
          apt install --no-install-recommends -y curl ca-certificates
        elif [ -x "$(command -v apk)" ]; then
          apk --no-cache add curl
        fi
      fi
      echo "Ensuring jq is available"
      if ! [ -x "$(command -v jq)" ]; then
        echo "jq binary not found attempting to install it"
        if [ -x "$(command -v apt)" ]; then
          apt update
          apt install --no-install-recommends -y jq
        elif [ -x "$(command -v apk)" ]; then
          apk --no-cache add jq
        fi
      fi
      echo "Discovering version file type"
      filename="${VERSION_FILE##*/}"
      ext=${filename##*.}
      case $ext in
        rb )
          echo "Ruby: version file found"
          version=$(ruby -e \
            "print Gem::Specification.load('${GEMSPEC_FILE}').version")
          [ -n "${version}" ] || \
            (echo "VERSION could not be parsed in ${GEMSPEC_FILE}!" && exit 1)
          ;;
        toml )
          echo "Rust: suspected cargo toml file found"
          version=$(cargo metadata --format-version=1 --no-deps --offline \
            | jq -r ".packages[] | select(.name == \"${PACKAGE_NAME}\") \
              .version")
          ;;
        h )
          echo "C/C++: suspected c/c++ header file found"
          version=$(gcc -E -dM ${VERSION_FILE} | sed -r "{ ${sed_script} }" )
          ;;
        yml )
          echo "YAML: suspected yaml file found"
          version=$(yq eval "${VERSION_VARIABLE}" ${VERSION_FILE})
          ;;
        json )
          echo "JSON: suspected json file found"
          version=$(jq -r "${VERSION_VARIABLE}" ${VERSION_FILE})
          ;;
        * )
          echo "I don't know how to parse a $ext file, please provide your" \
            "own parser that set a variable call version in a before_script" \
            "section"
          ;;
      esac
      [ -n "${version}" ] \
        || (echo "VERSION could not be parsed in ${VERSION_FILE}!" && exit 1)
      echo "Detected version: ${version}"
  script:
    - |
      echo "Updating changelog to version: ${version}"
      version="version=${version}"
      branch="branch=${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}"
      curl --request POST \
           --header "PRIVATE-TOKEN: ${CI_BOT_API_TOKEN}" \
           --data "${version}&${branch}" \
           "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/changelog"
