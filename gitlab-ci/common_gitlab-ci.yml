# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
---
variables:
  # Update this value when a new release is required
  # Format is X.Y.Z
  #   Increment X and reset Y and Z when changes require projects to be modified
  #   Increment Y and reset Z when adding new features
  #   Increment Z for bugfix only releases
  PIPELINE_TEMPLATE_VERSION: 1.4.2
  # These are needed as GitLab doesn't respect the container
  # entrypoint by default on kubernetes clusters
  FF_KUBERNETES_HONOR_ENTRYPOINT: 1
  FF_USE_LEGACY_KUBERNETES_EXECUTION_STRATEGY: 0

# Do not run a pipeline on a branch when a merge request is already open on it.
workflow:
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For `default` branch, create a pipeline (this includes on
    # schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'
    # For scheduled builds
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    # When manually run from the UI
    - if: '$CI_PIPELINE_SOURCE == "web"'
    # When run from a parent pipeline
    - if: '$CI_PIPELINE_SOURCE == "parent_pipeline"'
    # When run from a multi-project pipeline
    - if: '$CI_PIPELINE_SOURCE == "pipeline"'
    # A change to a branch, but a merge request is open for that branch,
    # do not run a branch pipeline
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    # For any commit
    - if: '$CI_COMMIT_BRANCH'

default:
  tags:
    - arm64
  before_script:
    - if [ "$CI_DEBUG_TRACE" == "true" ]; then
        env | sort;
      fi
  interruptible: true
  retry:
    max: 1
    when:
      - unknown_failure
      - api_failure
      - stuck_or_timeout_failure
      - runner_system_failure
      - unmet_prerequisites
      - scheduler_failure

.resource-request:
  tags:
    - arm64
  variables:
    KUBERNETES_CPU_REQUEST: '0.01'
    KUBERNETES_MEMORY_REQUEST: 128Mi
    KUBERNETES_EPHEMERAL_STORAGE_REQUEST: 1Gi

# Small script section that can be used to download files from the template repo
# if local versions are not present.
# It is not intended to be use via the "extends" keyword but by use of
#     - !reference [".fetch_file", script]
# within a jobs script block
# It assumes curl and jq are available and that FETCH_SCRIPT is set to the name
# of the script to fetch.
.fetch_file:
  extends: .resource-request
  script:
    - |
      if [ -f "${FETCH_SCRIPT}" ]; then
        script_file=${FETCH_SCRIPT}
      else
        script_file=$(basename "${FETCH_SCRIPT}")
        script_uri=$(printf %s "${FETCH_SCRIPT}" | jq  -sRr @uri)
        proj_api="${CI_API_V4_URL}/projects"
        proj_search="search=${PIPELINE_TEMPLATE_PROJECT}"
        proj_id=$(curl --header "PRIVATE-TOKEN: ${PRIVATE_CI_TOKEN}" \
          "${proj_api}?search_namespaces=true&${proj_search}" | \
          jq ".[] | select(.path_with_namespace | \
            contains(\"${PIPELINE_TEMPLATE_PROJECT}\")) | .id" )
        ref="ref=v${PIPELINE_TEMPLATE_VERSION}"

        curl --header "PRIVATE-TOKEN: ${PRIVATE_CI_TOKEN}" \
          "${proj_api}/${proj_id}/repository/files/${script_uri}/raw?${ref}" \
          > ${script_file}
      fi

stages:
  - Setup
  - Build
  - Test
  - Release
  - Cleanup
