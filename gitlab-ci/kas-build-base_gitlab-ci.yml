# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
---

.kas_build:
  extends:
    - .yocto_build
  image: ${MIRROR_GHCR}/siemens/kas/kas:latest-release
  variables:
    KAS_WORK_DIR: $CI_PROJECT_DIR/work
    KAS_REPO_REF_DIR: $CACHE_DIR/repos
    KAS_CONFIGS: $CI_JOB_NAME
    USER_ID: 999
    GROUP_ID: 999
  needs:
    - Update-Repos
  before_script:
    - !reference [".yocto_base", before_script]
    # Setup KAS workspace
    - echo KAS_WORK_DIR = $KAS_WORK_DIR
    - rm -rf $KAS_WORK_DIR
    - echo "Persistent cache volume could be empty, create directories required"
    - mkdir --verbose --parents
        $KAS_WORK_DIR
        $KAS_REPO_REF_DIR
  script:
    # Run build
    - echo "Building target $KAS_CONFIGS"
    - KASFILES=$(./.gitlab/scripts/jobs-to-kas ${KAS_CONFIGS})
    - echo KASFILES = $KASFILES
    - export BB_NUMBER_THREADS="${KUBERNETES_CPU_REQUEST}"
    - export PARALLEL_MAKE="-j ${KUBERNETES_CPU_REQUEST}"
    - kas shell --update --force-checkout $KASFILES -c 'cat conf/*.conf'
    - kas build $KASFILES;

Prune-Cache:
  extends: .kas_build
  needs: []

#
# Prep stage, update repositories once
#
Update-Repos:
  extends: .kas_build
  stage: Setup
  variables:
    UPDATE_REPO_SCRIPT: "./.gitlab/scripts/update-repos"
  needs: []
  script:
    - flock --verbose --timeout 60 "${KAS_REPO_REF_DIR}"
        "${UPDATE_REPO_SCRIPT}"

#
# Utility tasks, not executed automatically
#
Delete-Repo-Dir:
  extends: .kas_build
  stage: Setup
  allow_failure: true
  rules:
    - when: manual
  needs: []
  script:
    - rm -rf $KAS_REPO_REF_DIR/*

# Report on disk usage
Disk-Usage:
  extends: .kas_build
  stage: Setup
  needs: []
  script:
    - du -h -s $DL_DIR $SSTATE_DIR $TOOLCHAIN_DIR $KAS_REPO_REF_DIR
