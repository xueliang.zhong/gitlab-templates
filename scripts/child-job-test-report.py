# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Generates JUnit report for all child jobs

import json
import sys
import requests

from envparse import Env
from junit_xml import TestSuite, TestCase


def get_response(url, headers):
    """
    Call the GitLab API and return the json formatted result
    """
    response = requests.get(url, headers=headers)
    if not response.ok:
        try:
            generic_message = response.json().get('message', generic_message)
        except json.decoder.JSONDecodeError:
            generic_message = 'Invalid response from GitLab'
        sys.stderr.write(generic_message + '\n')
        exit(1)
    return response.json()


def get_test_results(headers, project_url, pipeline_id):
    """
    fetch a pipelines test report
    """
    report_url = f'{project_url}/pipelines/{pipeline_id}/test_report'

    return get_response(f'{report_url}', headers)


def generate_junit_test_case(test_suite_name, test_case_result):
    """
    Generate a Junit Test Case
    """
    test_case = TestCase(test_case_result['name'],
                         elapsed_sec=test_case_result['execution_time'],
                         classname=test_suite_name +
                         "." + test_case_result['classname'],
                         stdout=test_case_result['system_output'])

    if test_case_result['status'] == 'error':
        test_case.add_error_info("error")
    elif test_case_result['status'] == 'failed':
        test_case.add_failure_info("failed")
    elif test_case_result['status'] == 'skipped':
        test_case.add_skipped_info("skipped")

    return test_case


def generate_junit_test_suite(test_suite_name, test_suite_results):
    """
    Generate a Junit Test Suite
    """
    test_cases = []
    for test_case in test_suite_results:
        single_test_case = generate_junit_test_case(test_suite_name, test_case)
        test_cases.append(single_test_case)

    return test_cases


def generate_junit_file(test_results, filename):
    """
    Generate a Junit file from pipeline test results
    """
    test_suites = []
    for test_suite in test_results['test_suites']:
        test_cases = generate_junit_test_suite(test_suite['name'],
                                               test_suite['test_cases'])
        test_suites.append(TestSuite(test_suite['name'], test_cases))

    with open(filename, 'w') as f:
        TestSuite.to_file(f, test_suites)


def main():
    """
    download artifacts and place them in the jobs directory
    """
    env = Env()

    v4_origin = env('CI_API_V4_URL')
    project_id = env("CI_PROJECT_ID")
    pipeline_id = env('CI_PIPELINE_ID')
    headers = {
        'PRIVATE-TOKEN': env('PRIVATE_CI_TOKEN')
    }

    project_url = f'{v4_origin}/projects/{project_id}'
    page_url = f'{project_url}/pipelines/{pipeline_id}/bridges/'
    json_response = get_response(f'{page_url}', headers)

    for bridge in json_response:
        if bridge['downstream_pipeline']:
            bridge_id = bridge['downstream_pipeline']['id']
            test_results = get_test_results(headers,
                                            project_url,
                                            bridge_id)

            generate_junit_file(test_results, "TEST-ChildPipeline-" +
                                str(bridge_id) + ".xml")


if __name__ == '__main__':
    main()
